package ua.javabegin.training.hibernate.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ extends ua.javabegin.training.hibernate.entity.BaseEntity_ {

	public static volatile SingularAttribute<Book, Author> author;

	public static final String AUTHOR = "author";

}

