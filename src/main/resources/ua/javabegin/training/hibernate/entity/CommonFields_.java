package ua.javabegin.training.hibernate.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CommonFields.class)
public abstract class CommonFields_ {

	public static volatile SingularAttribute<CommonFields, String> name1;

	public static final String NAME1 = "name1";

}

