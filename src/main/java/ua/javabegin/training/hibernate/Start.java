package ua.javabegin.training.hibernate;

import org.apache.log4j.Logger;
import ua.javabegin.training.hibernate.entity.Book;


public class Start {
    private static final Logger LOG = Logger.getLogger(Start.class);

    public static void main(String[] args) {


        BookHelper bookHelper = new BookHelper();
        bookHelper.begin();
        AuthorHelper authorHelper = new AuthorHelper();

        LOG.warn("________________________________________");

      //  bookHelper.getBookList().get(0).getName();
       /* for (Book book:bookHelper.getBookList()){
            LOG.fatal(book);
        }*/
        LOG.fatal(authorHelper.getAuthor(1));
        authorHelper.getAuthor(1).getBooks().forEach(book -> LOG.fatal(book));
        LOG.warn("________________________________________");


        bookHelper.commit();

        LOG.trace("TRACE");
        LOG.debug("DEBUG");
        LOG.info("INFO");
        LOG.warn("WARNING");
        LOG.error("ERROR");
        LOG.fatal("FATAL");


        /*for (Author author : new AuthorHelper().getAuthorList()) {

            LOG.warn(author);
        }
        LOG.warn("___________________________________________");
        for (Book book : new BookHelper().getBookList()) {

            LOG.warn(book);
        }
*/



       /* authorHelper.begin();
        for (int i = 1; i <= 200; i++) {
            Author author = new Author();
            author.setName1("Name " + i);
            author.setLastName("Second_Name " + i);
            authorHelper.addAuthor(author);
        }
        authorHelper.commit();*/

    }
}
