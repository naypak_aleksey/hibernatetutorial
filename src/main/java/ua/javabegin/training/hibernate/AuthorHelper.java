package ua.javabegin.training.hibernate;

import lombok.Getter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ua.javabegin.training.hibernate.entity.Author;
import ua.javabegin.training.hibernate.entity.Author_;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import java.util.List;

@Getter
public class AuthorHelper {
    private SessionFactory sessionFactory;
    private Session session;

    public AuthorHelper() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
    }

    public List<Author> getAuthorList() {
        // открыть сессию для манипуляции с персистентными объектами
        Session session = sessionFactory.openSession();

        //получение объекта по Id
        session.get(Author.class, 1L);

        //объект-конструктор запросов для Criteria API
        CriteriaBuilder cb = session.getCriteriaBuilder();

        CriteriaQuery cq = cb.createQuery(Author.class);

        Root<Author> root = cq.from(Author.class);

        Selection[] selection = {root.get(Author_.id), root.get(Author_.COMMON_FIELDS)};

        ParameterExpression<String> nameParam = cb.parameter(String.class, "PARAMETER_NAME");


        cq.select(cb.construct(Author.class, selection)).where(cb.like(root.get(Author_.COMMON_FIELDS), nameParam));


        //выполнение запроса
        Query query = session.createQuery(cq);
        query.setParameter("PARAMETER_NAME", "%Ale%");

        List<Author> authorList = query.getResultList();

        session.close();

        return authorList;

    }

    public void delete() {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCriteriaBuilder();
        CriteriaDelete<Author> criteriaDelete = criteriaBuilder.createCriteriaDelete(Author.class);
        Root<Author> root = criteriaDelete.from(Author.class);
        ParameterExpression<String> nameParam = criteriaBuilder.parameter(String.class, "DELETE_PARAM");

        criteriaDelete.where(criteriaBuilder.notLike(root.get(Author_.COMMON_FIELDS), nameParam));

        Query query = session.createQuery(criteriaDelete);
        query.setParameter("DELETE_PARAM", "%1%");
        query.executeUpdate();

    }

    // add new author in table Author
    public Author addAuthor(Author author) {
        session.save(author);
        return author;
    }

    public Author getAuthor(long id) {
        Author author = session.get(Author.class, id);
       // author.getBooks().get(0).getName();
        return author;
    }

    public void commit() {
        session.getTransaction().commit();

        session.close();
    }

    public void begin() {
        session.beginTransaction();
    }
}
