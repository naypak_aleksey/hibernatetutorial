package ua.javabegin.training.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ua.javabegin.training.hibernate.entity.Book;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class BookHelper {
    private SessionFactory sessionFactory;
    private Query query;
    Session session;

    public BookHelper() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
    }


    public List<Book> getBookList() {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        Root<Book> root = cq.from(Book.class);
        cq.select(root);
        query = session.createQuery(cq);

        List<Book> bookList = query.getResultList();
        return bookList;
    }
    public void commit() {
        session.getTransaction().commit();

        session.close();
    }
    public void begin() {
        session.beginTransaction();
    }
}
