package ua.javabegin.training.hibernate.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Author implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @Embedded
    private CommonFields commonFields;

    @Column(name = "second_name")
    private String lastName;

    @OneToMany(fetch = FetchType.EAGER,targetEntity = Book.class,mappedBy = Book_.AUTHOR)//может быть автором нескольких книг
    private List<Book> books = new ArrayList<>();

}