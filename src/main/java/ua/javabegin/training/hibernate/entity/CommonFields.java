package ua.javabegin.training.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CommonFields {
    @Column(name = "name")
    private String name1;
}